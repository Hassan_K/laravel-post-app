@extends('structure.app')

@section('content')

    <div class="flex justify-center">
     <div class="w-4/12 p-9 bg-white rounded-lg">
      @if(session('status'))
     <div class="bg-red-100 p-4 border-red-400 text-red-900 mb-6 rounded-lg text-center">
       {{session('status')}}    
      </div>   
       @endif
     
        <form action="{{route('login')}}" method="POST">
        @csrf
        <div class="mb-4"> 
            <label for="email" class="sr-only">Email</label>
             <input type="text" name="email" id="email" placeholder="Your email" class="bg-gray-100  border-2 w-full p-4 rounded-lg @error('email')  border-red-500 @enderror" value="{{ old('email') }}">
       
             @error('email') <div class="text-red-500 mt-2 text-sm"> 
                {{ $message }} 
                </div> 
             @enderror
        </div>
        <div class="mb-4"> 
            <label for="password" class="sr-only">Password</label>
             <input type="password" name="password" id="password" placeholder="Your password" class="bg-gray-100  border-2 w-full p-4 rounded-lg @error('password')  border-red-500 @enderror"  value="">
       
             @error('password') <div class="text-red-500 mt-2 text-sm"> 
                {{ $message }} 
                </div> 
             @enderror
        </div>
        <div class="mb-4">
          <div class="flex items-center">
             <input type="checkbox" name="remember" id="remember" class="mr-2"> 
          <label for="remember">Remember me</label>
          </div> 
      </div>
   <div> 
        <button type="submit" class="bg-green-400 text-white px-4 py-3 rounded font-medium w-full">Login</button>
     </div>

        </form>
     </div>
    </div>
@endsection