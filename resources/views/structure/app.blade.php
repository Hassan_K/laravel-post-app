<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite('resources/css/app.css')
    <title>ww</title>
</head>
<body class="bg-green-200">
  
<nav class="p-6 bg-white flex justify-between mb-4" >
    <ul class="flex items-flex-center">

  
    <li>
            <a href="/" class="p-10">Home</a>
        </li>
        <li>
            <a href="{{route('dashboard')}}" class="p-10">Dashboard</a>
        </li>
        <li>
            <a href="" class="p-10">Post</a>
        </li>
    </ul>

 
    <ul class="flex items-flex-center">
        
    @if (auth()->user())
        <li>
            <a href="" class="p-10">{{ auth()->user()->name}}</a>
        </li>
        <li class="inline">
            <form action="{{route('logout')}}" method='POST' class ='p-10 inline' >
             @csrf
            <button type='submit' >Log out </button>
            </form>
        </li>
    @else
        <li>
            <a href="{{ route ('register') }}" class="p-10">Register</a>
        </li>
        <li>
            <a href="{{route('login')}}" class="p-10">Login </a>
        </li>
    @endif
    </ul>

 
   </nav>
    @yield('content')
</body>
</html>
